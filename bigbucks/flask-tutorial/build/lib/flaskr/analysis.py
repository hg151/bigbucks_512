import yfinance as yf
import yahooquery
from yahooquery import Ticker
import pandas as pd
import json
import numpy as np
import matplotlib.pyplot as plt
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from flaskr.db import get_db
from flaskr.myacct import get_user
from datetime import datetime, timedelta
from sklearn.linear_model import LinearRegression
import scipy.optimize as sc
# import plotly.graph_objects as go
# from pandas_datareader import data as pdr
risk_free_rate=0.0355

def get_stock_prices(ticker, start_date, end_date):
    data = yahooquery.Ticker(ticker).history(start=start_date, end=end_date)
    return data


def get_user_stock_tickers(user_id):
    query = "SELECT stock_ticker FROM account_stock WHERE user_id = ?"
    params = (user_id,)
    db = get_db()
    results = db.session.execute(query, params).fetchall()
    return [row[0] for row in results]


def get_user_stock_price_data(user_id, start_date, end_date):
    stock_tickers = get_user_stock_tickers(user_id)
    data = {}
    for ticker in stock_tickers:
        data[ticker] = get_stock_prices(ticker, start_date, end_date)
    return data


# retrieve the daily closing_price for a given stock_ticker from yahoo finance
# return a pandas DataFrame containing the closing prices
def get_closing_prices(ticker, start_date, end_date):
    data = yahooquery.Ticker(ticker).history(start=start_date, end=end_date)
    return data['close']

# retrieve the daily closing price for a given index ticker from yf
# return a pd DataFrame containing the closing price


def get_index_prices(ticker, start_date, end_date):
    data = yahooquery.Ticker('^{}'.format(ticker)).history(
        start=start_date, end=end_date)
    return data['close']

# calculate the daily simple returns for a given pandas DataFrame of closing prices


def calculate_simple_returns(ticker, data):
    returns = data.pct_change()
    returns = returns.dropna()
    returns = pd.DataFrame(returns, columns=['simple_return'])
    returns['ticker'] = ticker
    return returns


def get_user_simple_returns(user_id, start_date, end_date):
    stock_tickers = get_user_stock_tickers(user_id)
    returns = pd.DataFrame(columns=['date', 'simple_return', 'ticker'])
    for ticker in stock_tickers:
        prices = get_closing_prices(ticker, start_date, end_date)
        stock_returns = calculate_simple_returns(ticker, prices)
        returns = pd.concat([returns, stock_returns])
    return returns


bp = Blueprint('analysis', __name__)


# price plot:
# def generate_price_plot(ticker, data):
#     plt.figure(figsize=(10,5))
#     plt.plot(data['close'])
#     plt.xlabel('Date')
#     plt.ylabel('Price')
#     plt.title('Price Plot for {}'.format(ticker))
#     file_path = 'static/{}_price_plot.png'.format(ticker)
#     plt.savefig(file_path)
#     return file_path

@bp.route('/analysis/<int:id>/price_plot')
def account_price_plot(id):
    db = get_db()
    account = db.execute(
        "SELECT account_id FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
    accountstock_data = db.execute(
        "SELECT * FROM account_stock WHERE account_id=?", (account['account_id'],)).fetchall()
    # query Yahoo Finance to retrieve the stock price data
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices = yahooquery.Ticker(stock_tickers).history(
        start=start_date, end=end_date)['adjclose']
    # calculate the portfolio value time series
    weighted_prices = 0
    for i in stock_tickers:
        new_prices = stock_prices[i].multiply(
            [stock_weights[i]]*len(stock_prices[i]), axis=0)
        weighted_prices += new_prices
    print(weighted_prices)
    date = weighted_prices.index.values.tolist()
    for i in range(len(date)):
        date[i] = date[i].strftime('%Y-%m-%d')
    prices = weighted_prices.values[:].tolist()

    date = date
    info = {'date': date, 'prices': prices}
    # render the account_charts.html template with the necessary variables
    return render_template('analysis/plot1.html', info=info)

@bp.route('/analysis/<int:id>/daily_simple_return_plot')
def account_simple_returns(id):
    db = get_db()
    account = db.execute("SELECT account_id, balance FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
    accountstock_data = db.execute(
        "SELECT * FROM account_stock WHERE account_id=?", (account['account_id'],)).fetchall()
    # account_stock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (user_id,)).fetchall()
    # query Yahoo Finance to retrieve the stock price data
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
    # calculate the stock holdings
    weighted_prices = 0
    for i in stock_tickers:
        new_prices = stock_prices[i].multiply(
            [stock_weights[i]]*len(stock_prices[i]), axis=0)
        weighted_prices += new_prices
    print(weighted_prices)
    date = weighted_prices.index.values.tolist()[1:]
    for i in range(len(date)):
        date[i] = date[i].strftime('%Y-%m-%d')
    prices = weighted_prices.values[:].tolist()
    returns = [0]*(len(prices)-1)
    for i in range(0,len(prices)-1):
        returns[i] = (prices[i+1] - prices[i]) / prices[i] * 100
    info = {'date': date, 'prices': returns}
    # render the account_charts.html template with the necessary variables
    return render_template('analysis/plot2.html', info = info)

@bp.route('/analysis/<int:id>/today_vs_yesterday_simple_returns')
def account_today_vs_yesterday_simple_returns(id):
    db = get_db()
    # query the database to retrieve the user's account and stock data
    account = db.execute("SELECT account_id, balance FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
    accountstock_data = db.execute(
        "SELECT * FROM account_stock WHERE account_id=?", (account['account_id'],)).fetchall()
    # query Yahoo Finance to retrieve the stock price data for today and yesterday
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    end_date_t = datetime.now().strftime('%Y-%m-%d')
    start_date_t = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices_today = yahooquery.Ticker(stock_tickers).history(start=start_date_t, end=end_date_t, interval='1d')['adjclose']
    start_date_y = (datetime.now() - timedelta(days=366)).strftime('%Y-%m-%d')
    end_date_y = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    stock_prices_yesterday = yahooquery.Ticker(stock_tickers).history(start=start_date_y, end=end_date_y, interval='1d')['adjclose']
    
    weighted_prices_yesterday = 0
    for i in stock_tickers:
        new_prices = stock_prices_yesterday[i].multiply(
            [stock_weights[i]]*len(stock_prices_yesterday[i]), axis=0)
        weighted_prices_yesterday += new_prices
    prices_yesterday = weighted_prices_yesterday.values[:].tolist()
    
    weighted_prices_today = 0
    for i in stock_tickers:
        new_prices = stock_prices_today[i].multiply(
            [stock_weights[i]]*len(stock_prices_today[i]), axis=0)
        weighted_prices_today += new_prices
    prices_today = weighted_prices_today.values[:].tolist()

    return_yesterday = [0]*(len(prices_yesterday)-1)
    for i in range(0,len(prices_yesterday)-1):
        return_yesterday[i] = (prices_yesterday[i+1] - prices_yesterday[i]) / prices_yesterday[i] * 100

    return_today = [0]*(len(prices_today)-1)
    for i in range(0,len(prices_today)-1):
        return_today[i] = (prices_today[i+1] - prices_today[i]) / prices_today[i] * 100

    info = {'yesterday': return_yesterday, 'today': return_today}
    
    # render the account_charts.html template with the necessary variables
    return render_template('analysis/plot3.html', info=info)

@bp.route('/analysis/<int:id>/simple_return_histogram')
def account_simple_return_histogram(id):
    # query the database to retrieve the user's account and stock data
    db = get_db()
    account = db.execute("SELECT account_id, balance FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
    accountstock_data = db.execute(
        "SELECT * FROM account_stock WHERE account_id=?", (account['account_id'],)).fetchall()
    # account_stock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (user_id,)).fetchall()
    # query Yahoo Finance to retrieve the stock price data
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
    weighted_prices = 0
    for i in stock_tickers:
        new_prices = stock_prices[i].multiply(
            [stock_weights[i]]*len(stock_prices[i]), axis=0)
        weighted_prices += new_prices
    
    prices = weighted_prices.values[:].tolist()
    returns = [0]*(len(prices)-1)
    for i in range(0,len(prices)-1):
        returns[i] = (prices[i+1] - prices[i]) / prices[i] * 100
    
    info = {'returns': returns}
    
    return render_template('analysis/plot4.html', info=info)

@bp.route('/analysis/<int:id>/stock_vs_index')
def account_stock_vs_index(id):
    db = get_db()
    # query the database to retrieve the user's account and stock data
    accountstock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (id,)).fetchall()
    # query Yahoo Finance to retrieve the stock and index price data
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
    
    SPY_prices = yahooquery.Ticker(['SPY']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    DJI_prices = yahooquery.Ticker(['DJIA']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    COMP_prices = yahooquery.Ticker(['COMP']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    # generate the stock price versus index price chart using bokeh
    
    weighted_prices = 0
    for i in stock_tickers:
        new_prices = stock_prices[i].multiply(
            [stock_weights[i]]*len(stock_prices[i]), axis=0)
        weighted_prices += new_prices
    
    stock_prices = (weighted_prices.values[:] / weighted_prices.values[0]).tolist()
    date = weighted_prices.index.values.tolist()
    for i in range(len(date)):
        date[i] = date[i].strftime('%Y-%m-%d')
    SPY_prices = (SPY_prices.values[:] / SPY_prices.values[0]).tolist()
    DJI_prices = (DJI_prices.values[:] / DJI_prices.values[0]).tolist()
    COMP_prices = (COMP_prices.values[:] / COMP_prices.values[0]).tolist()
    info = {'date': date, 'stock_prices': stock_prices, 'SPY': SPY_prices, 'DJI': DJI_prices, 'COMP': COMP_prices}

    return render_template('analysis/plot5.html', info=info)

# stock return versus index return time series chart
@bp.route('/analysis/<int:id>/stock_return_vs_index_return')
def account_stock_return_vs_index_return(id):
    db = get_db()
    # query the database to retrieve the user's account and stock data
    accountstock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (id,)).fetchall()
    # query Yahoo Finance to retrieve the stock and index price data
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])

    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
    
    SPY_prices = yahooquery.Ticker(['SPY']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    DJI_prices = yahooquery.Ticker(['DJIA']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    COMP_prices = yahooquery.Ticker(['COMP']).history(start=start_date, end=end_date, interval='1d')['adjclose']

    weighted_prices = 0
    for i in stock_tickers:
        new_prices = stock_prices[i].multiply(
            [stock_weights[i]]*len(stock_prices[i]), axis=0)
        weighted_prices += new_prices
    
    date = weighted_prices.index.values.tolist()
    stock_prices = weighted_prices.values[:].tolist()
    stock_returns = [0]*(len(stock_prices)-1)
    for i in range(0,len(stock_prices)-1):
        stock_returns[i] = (stock_prices[i+1] - stock_prices[i]) / stock_prices[i] * 100

    SPY_prices = SPY_prices.values[:].tolist()
    SPY_returns = [0]*(len(SPY_prices)-1)
    for i in range(0,len(SPY_prices)-1):
        SPY_returns[i] = (SPY_prices[i+1] - SPY_prices[i]) / SPY_prices[i] * 100

    DJI_prices = DJI_prices.values[:].tolist()
    DJI_returns = [0]*(len(DJI_prices)-1)
    for i in range(0,len(DJI_prices)-1):
        DJI_returns[i] = (DJI_prices[i+1] - DJI_prices[i]) / DJI_prices[i] * 100

    COMP_prices = COMP_prices.values[:].tolist()
    COMP_returns = [0]*(len(COMP_prices)-1)
    for i in range(0,len(COMP_prices)-1):
        COMP_returns[i] = (COMP_prices[i+1] - COMP_prices[i]) / COMP_prices[i] * 100
    
    info = {'date': date, 'stock_returns': stock_returns, 'SPY': SPY_returns, 'DJI': DJI_returns, 'COMP': COMP_returns}
    return render_template('analysis/plot6.html',info = info)

## NOT WORKING YET!!
# scatter plot of stock return versus index return:
@bp.route('/analysis/<int:id>/scatter_stock_return_vs_index_return')
def scatter_account_stock_vs_index(id):
    # query the database to retrieve the user's account and stock data
    db = get_db()
    accountstock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (id,)).fetchall()
    # query Yahoo Finance to retrieve the stock and index price data
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    stock_weights = {}
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')
    stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
    
    SPY_prices = yahooquery.Ticker(['SPY']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    # DJI_prices = yahooquery.Ticker(['DJIA']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    # COMP_prices = yahooquery.Ticker(['COMP']).history(start=start_date, end=end_date, interval='1d')['adjclose']
    
    weighted_prices = 0
    for i in stock_tickers:
        new_prices = stock_prices[i].multiply(
            [stock_weights[i]]*len(stock_prices[i]), axis=0)
        weighted_prices += new_prices
    stock_prices = weighted_prices.values[:].tolist()
    stock_returns = [0]*(len(stock_prices)-1)
    for i in range(0,len(stock_prices)-1):
        stock_returns[i] = (stock_prices[i+1] - stock_prices[i]) / stock_prices[i]

    SPY_prices = SPY_prices.values[:].tolist()
    SPY_returns = [0]*(len(SPY_prices)-1)
    for i in range(0,len(SPY_prices)-1):
        SPY_returns[i] = (SPY_prices[i+1] - SPY_prices[i]) / SPY_prices[i]

    model = LinearRegression()
    model.fit(np.array(SPY_returns).reshape(-1, 1), np.array(stock_returns).reshape(-1, 1))

    alpha = model.coef_
    beta = model.intercept_

    x = np.linspace(-0.07, 0.07, 100)
    y = alpha * x + beta
    x = x.tolist()
    y = y.tolist()[0]

    info = {'SPY':SPY_returns, "stock": stock_returns, "x": x, "y":y}
    
    return render_template('analysis/plot7.html', info = info)

## NOT WORKING YET!!!
def get_current_price(ticker):
    end_date = (datetime.now()).strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
    current_price = yahooquery.Ticker(ticker).history(start=start_date, end=end_date, interval='1d')
    if not current_price.empty:
        current_price = current_price['close']
    else:
        start_date_alternative = (datetime.now() - timedelta(days=2)).strftime('%Y-%m-%d')
        current_price = yahooquery.Ticker(ticker).history(start=start_date_alternative, end=end_date, interval='1d')
        if not current_price.empty:
            current_price = current_price['close']
        else:
            start_date_alternative = (datetime.now() - timedelta(days=3)).strftime('%Y-%m-%d')
            current_price = yahooquery.Ticker(ticker).history(start=start_date_alternative, end=end_date, interval='1d')
            if not current_price.empty:
                current_price = current_price['close']
            else:
                start_date_alternative = (datetime.now() - timedelta(days=4)).strftime('%Y-%m-%d')
                current_price = yahooquery.Ticker(ticker).history(start=start_date_alternative, end=end_date, interval='1d')
            if not current_price.empty:
                current_price = current_price['close']
            else:
                start_date_alternative = (datetime.now() - timedelta(days=5)).strftime('%Y-%m-%d')
                current_price = yahooquery.Ticker(ticker).history(start=start_date_alternative, end=end_date, interval='1d')['close']
               
        return current_price


# efficient frontier:
def portfolioreturn(df, weights):
    return np.dot(df.mean(),weights)*np.sqrt(252)
  
def portfoliostd(df, weights):
    return (np.dot(np.dot(df.cov(), weights), weights))**(1/2)*np.sqrt(252)

def weightscreator(df):
    rand = np.random.random(len(df.columns))
    rand /= rand.sum()
    return rand

def ef_graph(df):
    returns = []
    stds = []
    w = []
    for i in range(2000):
        weights = weightscreator(df)
        
        returns.append(portfolioreturn(df, weights))
        stds.append(portfoliostd(df, weights))
        w.append(weights)
    return stds, returns

@bp.route('/analysis/<int:id>/efficient frontier')
def get_data(id):
  db = get_db()
  account = db.execute(
        "SELECT account_id FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
  account_stock_data = db.execute(
        "SELECT * FROM account_stock WHERE account_id=?", (account['account_id'],)).fetchall()
  stock_tickers = [s['stock_ticker'] for s in account_stock_data]
#   stock_weights = {}
#   weights = []
#   total_amount = 0
#   for s in account_stock_data:
#       total_amount += float(s['transaction_number'])
#   for s in account_stock_data:
#     stock_weights[s['stock_ticker']] = float(s['transaction_number'] / total_amount)
#     weights.append(stock_weights[s['stock_ticker']])

  end_date = datetime.now().strftime('%Y-%m-%d')
  start_date = (datetime.now() - timedelta(days=3650)).strftime('%Y-%m-%d')

  df = yf.download(stock_tickers, start=start_date, end=end_date)
  df = np.log(1 + df['Adj Close'].pct_change())

  stds, returns = ef_graph(df)
#   stds, returns = ef_graph(df)
  x = stds
  y = returns
  opt_std = min(stds)
  opt_return = returns[stds.index(min(stds))]
  
  info = {'x':x,'y':y, 'opt_x': opt_std, 'opt_y': opt_return}
#   optimal = {'x':opt_std, 'y': opt_return}
  return render_template('analysis/ef.html', info = info)
  
# @bp.route('/analysis/<int:id>/')
# def analysis_index(id):
#     db = get_db()
#     account = db.execute("SELECT account_id FROM account JOIN user ON account.user_id=user.id WHERE user.id=?", (id,)).fetchone()
#     accountstock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (id,)).fetchall()
#     # query Yahoo Finance to retrieve the stock and index price data
#     stock_tickers = [s['stock_ticker'] for s in accountstock_data]
#     # stock_weights = {}
#     # weights = []
#     # for s in accountstock_data:
#     #     stock_weights[s['stock_ticker']] = float(s['transaction_number'])
#     #     weights.append(float(s['transaction_number']))
#     stock_weights = {}
#     weights = []
#     total_amount = 0
#     for s in accountstock_data:
#         total_amount += float(s['transaction_number'])
#     print(total_amount)
#     for s in accountstock_data:
#         stock_weights[s['stock_ticker']] = float(s['transaction_number'] / total_amount)
#         weights.append(stock_weights[s['stock_ticker']])
#     weights = np.array(weights)
#     print("**************************")
#     print("**************************")
#     print("**************************")
#     print("**************************")
#     print(weights)
#     end_date = datetime.now().strftime('%Y-%m-%d')
#     start_date = (datetime.now() - timedelta(days=3650)).strftime('%Y-%m-%d')
#     stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
#     risk_free = 0.0355

#     returns = []
#     stds = []
#     vectors = []
#     for i in stock_tickers:
#        init_price = stock_prices[i].to_frame()
#        ptc = init_price.pct_change()[1:]
#        vectors.append(ptc.values)
#        avg = ptc.mean()
#        nor_avg = avg.values[0]*(len(ptc)+1)
#        returns.append(nor_avg)
#        std = ptc.std()
#        nor_std = std.values[0]*(len(ptc)+1)**0.5
#        stds.append(nor_std)
    
#     returns = np.array(returns)
#     stds = np.array(stds)
#     print("**************************")
#     print("**************************")
#     print("**************************")
#     print("**************************")
#     print(returns)
#     portfolio_return = np.sum(returns * weights)
#     print("**************************")
#     print(portfolio_return)
#     comb_vector = np.stack((i.ravel() for i in vectors), axis=0)
#     covariance_matrix = np.cov(comb_vector)
#     portfolio_std = (np.dot(np.dot(df.cov(), weights), weights))**(1/2)*np.sqrt(252)
#     portfolio_std = np.sqrt(np.dot(weights.T, np.dot(covariance_matrix, weights)))
#     print("**************************")
#     print(portfolio_std)
#     sharpe_ratio = (portfolio_return - risk_free) / portfolio_std

#     return render_template('analysis/index.html', sharpe_ratio=sharpe_ratio)

@bp.route('/analysis/<int:id>/')
def analysis_index(id):
    db = get_db()
    account = db.execute("SELECT account_id FROM account JOIN user ON account.user_id=user.id WHERE user.id=?", (id,)).fetchone()
    accountstock_data = db.execute("SELECT * FROM account_stock WHERE user_id = ?", (id,)).fetchall()
    # query Yahoo Finance to retrieve the stock and index price data
    stock_tickers = [s['stock_ticker'] for s in accountstock_data]
    # stock_weights = {}
    # weights = []
    # for s in accountstock_data:
    #     stock_weights[s['stock_ticker']] = float(s['transaction_number'])
    #     weights.append(float(s['transaction_number']))
    stock_weights = {}
    weights = []
    total_amount = 0
    for s in accountstock_data:
        total_amount += float(s['transaction_number'])
    print(total_amount)
    for s in accountstock_data:
        stock_weights[s['stock_ticker']] = float(s['transaction_number'] / total_amount)
        weights.append(stock_weights[s['stock_ticker']])
    weights = np.array(weights)
    print("**************************")
    print("**************************")
    print("**************************")
    print("**************************")
    print(weights)
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=3650)).strftime('%Y-%m-%d')
    # stock_prices = yahooquery.Ticker(stock_tickers).history(start=start_date, end=end_date, interval='1d')['adjclose']
    risk_free = 0.0355
    df = yf.download(stock_tickers, start=start_date, end=end_date)
    df = np.log(1 + df['Adj Close'].pct_change())
    print(df)
    portfolio_return = np.dot(df.mean(),weights)*np.sqrt(252)
    portfolio_std = (np.dot(np.dot(df.cov(), weights), weights))**(1/2)*np.sqrt(252)
    
    print("**************************")
    print("**************************")
    print("**************************")
    print("**************************")
    print(portfolio_return)
    print("**************************")
    print(portfolio_std)
    sharpe_ratio = (portfolio_return - risk_free) / portfolio_std

    return render_template('analysis/index.html', sharpe_ratio=sharpe_ratio)
    

@bp.route('/analysis/<int:id>/piechart')
def piechart(id):
    user = get_user(id)

    if request.method == 'GET':
        db = get_db()
        try:
            account = db.execute(
                "SELECT balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
                    id,)
            ).fetchone()
            account_stocks = db.execute(
                'SELECT account_stock.user_id, account_stock.account_id, account_stock.stock_ticker, account_stock.transaction_number, account_stock.cost '
                'FROM account_stock '
                'WHERE account_stock.user_id = ?', (id,)
            ).fetchall()
            num_stocks = len(account_stocks)
            print(
                f"{num_stocks} transactions fetched from the database.account stocks.")
            print(f"user id is {id}")
            stocks = []
            total_value = 0
            balance = account['balance']
            # charting for the pie chart
            # stocks_info = {'labels':[], 'weights':[], 'values':[]}
            labels = []
            weights = []
            values = []
            for i in account_stocks:
                symbol = i['stock_ticker']
                labels.append(symbol)
                ticker = Ticker(symbol)
                current_price = ticker.price[symbol]['regularMarketPrice']
                i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
                i_dict['current_price'] = current_price
                i_dict['total_value'] = current_price * float(i['transaction_number'])
                weights.append(float(i['transaction_number']))
                values.append(current_price * float(i['transaction_number']))
                total_value += current_price * float(i['transaction_number'])
                stocks.append(i_dict)
                print(i_dict)
        
            stocks_info = {'labels':labels, 'weights':weights, 'values': values}
            return render_template('analysis/piechart.html', user=user, stocks=stocks,total_value=total_value,balance=balance, stocks_info=stocks_info)

        except Exception as e:
            error = f"account stocks select. Error creating account: {str(e)}"
            print(error)
            db.rollback()
        print("before return")
        

        return render_template('error/error.html')




    