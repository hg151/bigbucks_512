import sqlite3
import os
import click
from flask.cli import with_appcontext
from flask import current_app, g


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


# def init_db():
#     db = get_db()
#     os.environ['TZ'] = 'US/Eastern'
#     with current_app.open_resource('schema.sql') as f:
#         db.executescript(f.read().decode('utf8'))

def init_db():
    db = get_db()
    with current_app.app_context():
        with db as c:
            with current_app.open_resource('schema.sql') as f:
                c.executescript(f.read().decode('utf8'))
        db.commit()

@click.command('init-db')
@with_appcontext
def init_db_command():
    """
    Clear the existing data and create new tables.
    """
    init_db()
    click.echo('Initialized the database.')


# @click.command('init-db')
# def init_db_command():
#     init_db()
#     click.echo('Initialized the database.')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
