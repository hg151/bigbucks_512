from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.auth import login_required
from flaskr.db import get_db
from datetime import datetime, timedelta
import yfinance as yf
import numpy as np
import yahooquery
from yahooquery import Ticker

bp = Blueprint('admin', __name__)


@bp.route('/allstock')
def all_stock():
    db = get_db()
    try:
        market_stocks = db.execute(
            'SELECT stock_ticker, SUM(transaction_number) AS total '
            'FROM account_stock '
            'GROUP BY stock_ticker;'
        ).fetchall()
        print(market_stocks['total'])
    except Exception as e:
        error = f"transactions select. Error creating account: {str(e)}"
        print(error)
        db.rollback()
    return render_template('admin/allstock.html', stocks=market_stocks)


@bp.route('/alltransactions', methods=('GET', 'POST'))
def all_transactions():

    if request.method == 'GET':
        db = get_db()
        try:
            transactions = db.execute('SELECT * FROM transactions;').fetchall()
            db.commit()
        except Exception as e:
            error = f"transactions select. Error finding transactions: {str(e)}"
            print(error)
            db.rollback()

        return render_template('admin/alltransactions.html', transactions=transactions)
    
    # transactions = None

    if request.method == 'POST':
        date = request.form['date']
        error = None

        if not date:
            error = 'Please pick a date.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            try:
                transactions = db.execute(
                    'SELECT * FROM transactions WHERE DATE(trade_time) = ?;', (date,)
                ).fetchall()
                db.commit()
            except Exception as e:
                error = f"transactions select. Error finding transactions: {str(e)}"
                print(error)
                db.rollback()

    return render_template('admin/alltransactions.html', transactions=transactions)

# Total risk-return (ef)
def portfolioreturn(df, weights):
    return np.dot(df.mean(),weights)*np.sqrt(252)
  
def portfoliostd(df, weights):
    return (np.dot(np.dot(df.cov(), weights), weights))**(1/2)*np.sqrt(252)

def weightscreator(df):
    rand = np.random.random(len(df.columns))
    rand /= rand.sum()
    return rand

def ef_graph(df):
    returns = []
    stds = []
    w = []
    for i in range(2000):
        weights = weightscreator(df)
        returns.append(portfolioreturn(df, weights))
        stds.append(portfoliostd(df, weights))
        w.append(weights)
    return stds, returns

@bp.route('/riskreturn')
def total_ef():
    db = get_db()
    
    market_stocks = db.execute(
        'SELECT stock_ticker, SUM(transaction_number) AS total '
        'FROM account_stock '
        'GROUP BY stock_ticker;'
        ).fetchall()
    all_tickers = [s['stock_ticker'] for s in market_stocks]
    print(all_tickers)
    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365)).strftime('%Y-%m-%d')

    df = yf.download(all_tickers, start=start_date, end=end_date)
    df = np.log(1 + df['Adj Close'].pct_change())

    stds, returns = ef_graph(df)
#   stds, returns = ef_graph(df)
    x=stds
    print("x ----------------------------")
    print(x)
    print("y -----------------------------")
    y=returns
    print(y)
    opt_std = min(stds)
    opt_return = returns[stds.index(min(stds))]
  
    info = {'x':x,'y':y, 'opt_x': opt_std, 'opt_y': opt_return}
    # info = {'x':x,'y':y}
    
    return render_template('admin/riskreturn.html', info=info)

@bp.route('/totalsharperatio')
def total_SR():
    db = get_db()
    market_stocks = db.execute(
        'SELECT stock_ticker, SUM(transaction_number) AS total '
        'FROM account_stock '
        'GROUP BY stock_ticker;'
        ).fetchall()
    
    all_tickers = [s['stock_ticker'] for s in market_stocks]
    stock_weights = {}
    weights = []
    # for s in market_stocks:
    #     stock_weights[s['stock_ticker']] = float(s['total'])
    #     weights.append(float(s['total']))
    total_amount = 0
    for s in market_stocks:
        total_amount += float(s['total'])
    print(total_amount)
    for s in market_stocks:
        stock_weights[s['stock_ticker']] = float(s['total'] / total_amount)
        weights.append(stock_weights[s['stock_ticker']])
    weights = np.array(weights)

    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=3650)).strftime('%Y-%m-%d')
    risk_free = 0.0355
    df = yf.download(all_tickers, start=start_date, end=end_date)
    df = np.log(1 + df['Adj Close'].pct_change())
    print(df)
    portfolio_return = np.dot(df.mean(),weights)*np.sqrt(252)
    portfolio_std = (np.dot(np.dot(df.cov(), weights), weights))**(1/2)*np.sqrt(252)
    
   
    sharpe_ratio = (portfolio_return - risk_free) / portfolio_std
    
    return render_template('admin/totalsharperatio.html', sharpe_ratio=sharpe_ratio)
