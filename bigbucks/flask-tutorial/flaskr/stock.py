from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, get_flashed_messages
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

from yahooquery import Ticker

import yahooquery as yq

import pandas as pd

from datetime import datetime, timedelta

import json

import numpy as np

bp = Blueprint('stock', __name__)


@bp.route('/search', methods=('GET', 'POST'))
def search():
    if request.method == 'POST':
        symbol = request.form['ticker']
        error = None

        if not symbol:
            error = 'Ticker is required.'

        if error is not None:
            flash(error)
            return redirect(request.referrer)
            
        else:
            # print("I try to find the stock")
            # db = get_db()
            # stock = get_db().execute(
            #     'SELECT stock_ticker, company_name, company_address, company_brief, current_price, pe_ratio'
            #     ' FROM stock'
            #     ' WHERE stock_ticker = ?',
            #     (symbol,)
            # ).fetchone()

            # print(stock)

            # if not stock:

            #     print("I try to add new stock to the table")
            #     ticker = Ticker(symbol)
            #     company_name = ticker.price[symbol]['longName']
            #     company_brief = ticker.asset_profile[symbol]['longBusinessSummary']
            #     current_price = ticker.price[symbol]['regularMarketPrice']
            #     company_address = ticker.asset_profile[symbol]['address1']
            #     pe_ratio = ticker.index_trend[symbol]['peRatio']

            #     # error handling required
            #     try:
            #         db = get_db()
            #         db.execute(
            #             "INSERT INTO stock(stock_ticker, company_name, company_address, company_brief, current_price, pe_ratio) VALUES(?, ?, ?, ?, ?, ?)",
            #             (symbol, company_name, company_address,
            #              company_brief, current_price, pe_ratio)
            #         )
            #         db.commit()
            #     except Exception as e:
            #         error = f"Error creating account: {str(e)}"

            # return redirect(url_for('blog.index'))

            end_date = datetime.now().strftime('%Y-%m-%d')
            df = yq.Ticker(symbol).history(start='2010-01-01', end=end_date)
            print("***********************")
            print(df)
            if df.empty:
                print("You are in df.empty")
                flash("Please input a valid ticker!")
                # messages = get_flashed_messages()
                # return render_template('error/error.html')
                return redirect(request.referrer)


            data = df['open'].index.values[:].tolist()
            dates = []  # List to store the extracted date values

            for symbol, date in data:
                # print(date)
                dates.append(date.strftime('%Y-%m-%d'))
            df['date'] = dates
            # Loop through each row in the DataFrame and create a dictionary for each row
            data = []
            # print(df)
            for index, row in df.iterrows():
                data.append([row['date'],float(row['open']),float(row['high']),float(row['low']),float(row['close'])])

            # Convert the list of dictionaries to a JSON object
            # json_data = pd.Series(data).to_json(orient='records')
            json_data = data
            stock_ticker = symbol
            return render_template('stock/search.html', json_data=json_data, stock_ticker=stock_ticker)

    symbol = 'SPY'
    end_date = datetime.now().strftime('%Y-%m-%d')
    df = yq.Ticker(symbol).history(start='2010-01-01', end=end_date)
    data = df['open'].index.values[:].tolist()
    dates = []  # List to store the extracted date values

    for symbol, date in data:
        # print(date)
        dates.append(date.strftime('%Y-%m-%d'))
    df['date'] = dates
    # Loop through each row in the DataFrame and create a dictionary for each row
    data = []
    # print(df)
    for index, row in df.iterrows():
        data.append([row['date'],float(row['open']),float(row['high']),float(row['low']),float(row['close'])])

        # Convert the list of dictionaries to a JSON object
    # json_data = pd.Series(data).to_json(orient='records')
    json_data = data
    # print(data)
    stock_ticker = 'S&P 500'
    return render_template('stock/search.html',json_data=json_data, stock_ticker=stock_ticker)


def get_user(id):
    user = get_db().execute(
        'SELECT id, username, email, phone, usertype, userstatus'
        ' FROM user'
        ' WHERE id = ?',
        (id,)
    ).fetchone()

    if user is None:
        abort(404, f"Post id {id} doesn't exist.")

   # if check_user and post['author_id'] != g.user['id']:
    #    abort(403)

    return user


@bp.route('/<int:id>/buy', methods=('GET', 'POST'))
@login_required
def buy(id):
    user = get_user(id)

    if request.method == 'POST':
        symbol = request.form['ticker']
        amount = int(request.form['amount'])
        error = None

        if not symbol:
            error = 'Ticker is required.'

        if not amount:
            error = 'Amount is required.'

        if error is not None:
            flash(error)
            return redirect(request.referrer)
        else:
            # calculate the total price of transaction
            ticker = Ticker(symbol)

            df = ticker.history(start='2022-01-01', end='2023-01-01')
            if df.empty:
                flash('Please input a valid ticker!')
                return redirect(request.referrer)
                # return render_template('error/error.html')
            
            current_price = ticker.price[symbol]['regularMarketPrice']
            total_price = current_price * amount
            print("In buy, the total price is " + str(total_price))
            db = get_db()
            account = db.execute(
                "SELECT account_id, balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
                    id,)
            ).fetchone()
            if float(account['balance']) >= total_price:
                # add the stock to the portfolio
                new_balance = float(account['balance']) - total_price
                db = get_db()
                try:
                    db.execute(
                        "UPDATE account SET balance = ? WHERE user_id = ?;",
                        (new_balance, id)
                    )
                except Exception as e:
                    error = f"account. Error creating account: {str(e)}"
                    db.rollback()

            # if we don't use buy price, this logic is not enough
            # first, find out if the stock ticker exists in the database
            # if exists, then update
            # if not, then insert into the table
            # delete the buy price

                try:
                    # first, find out if the stock ticker exists in the database
                    result = db.execute(
                        "SELECT * FROM account_stock JOIN user ON account_stock.user_id = user.id WHERE account_stock.user_id = ? AND stock_ticker = ?;", (id, symbol))
                    existing_record = result.fetchone()
                    # if not, then insert into the table
                    if existing_record == None:
                        try:
                            db.execute(
                                "INSERT INTO account_stock(user_id, account_id, stock_ticker, transaction_number, cost) VALUES (?, ?, ?, ?, ?);",
                                (id, account['account_id'],
                                 symbol, amount, float(total_price))
                            )
                            db.commit()
                        except Exception as e:
                            error = f"Account_stock. Can not insert: {str(e)}"
                            print(error)
                            print("AN ERROR OCCURS HERE.")
                            db.rollback()

                    # if exists, then update
                    else:
                        db.execute(
                            "UPDATE account_stock SET transaction_number = transaction_number + ?, cost = cost + ? WHERE user_id = (SELECT id FROM user WHERE id = ?) AND stock_ticker = ?;", (amount, float(total_price), id, symbol))
                        db.commit()
                except Exception as e:
                    error = f"Account_stock. Error creating account: {str(e)}"
                    print(error)
                    print("THE PROBLEM IS HERE.")
                    db.rollback()

                # add the transaction to the transaction table
                try:
                    db.execute(
                        "INSERT INTO transactions(user_id, account_id, stock_ticker, trade_type, transaction_number, buy_price, price_per_share) VALUES (?, ?, ?, ?, ?, ?, ?);",
                        (id, account['account_id'], symbol,
                         'Buy', amount, total_price, current_price)
                    )
                    db.commit()
                except Exception as e:
                    error = f"Transactions. Error creating account: {str(e)}"
                    print(error)
                    db.rollback()

                return redirect(url_for('stock.success', id=g.user['id']))

            else:
                error = "Insufficient balance! Please charge before your purchase."
                flash(error)
                return redirect(request.referrer)

    return render_template('stock/buy.html', user=user)


@bp.route('/<int:id>/<stock_ticker>/sell', methods=('GET', 'POST'))
@login_required
def sell(id, stock_ticker):
    print(stock_ticker)
    user = get_user(id)
    db = get_db()

    if request.method == 'POST':
        # stock_ticker = request.form['ticker']
        amount = int(request.form['amount'])
        error = None

        if not amount:
            error = 'Amount is required.'

        if error is not None:
            flash(error)
            return redirect(request.referrer)
        else:
            ticker = Ticker(stock_ticker)
            current_price = ticker.price[stock_ticker]['regularMarketPrice']
            total_price = current_price * amount
            print("Amount is " + str(amount))
            print("Cost is " + str(total_price))
            account = db.execute(
                "SELECT account_id, balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
                    id,)
            ).fetchone()
            # fetch the existing row from the account_stock table
            try:
                result = db.execute(
                    "SELECT * FROM account_stock WHERE stock_ticker = ? AND user_id = ?;", (stock_ticker,id)).fetchone()
                print("*************")
                print(float(result['transaction_number']) > float(amount))
                new_cost = float(result['cost']) - float(result['cost']) / float(result['transaction_number']) * float(amount)
                new_income = total_price - float(result['cost']) / float(result['transaction_number']) * float(amount)

                if float(result['transaction_number']) > float(amount):
                    # update the stock amount hold
                    try:
                        db.execute(
                            "UPDATE account_stock SET transaction_number = transaction_number - ?, cost = ? WHERE stock_ticker = ? AND user_id = ?;", (
                                amount, new_cost, stock_ticker,id)
                        )
                        db.commit()
                    except Exception as e:
                        error = f"Account_stock. Error update the transaction number: {str(e)}"
                        print(error)
                        print("AN ERROR OCCURS HERE.")
                        db.rollback()
                    try:
                        db.execute(
                            "UPDATE account SET balance = balance + ?, income += ? WHERE user_id = ?;",
                            (total_price, new_income, id)
                        )
                        db.commit()
                    except Exception as e:
                        error = f"account. Error creating account: {str(e)}"
                        db.rollback()

                elif float(result['transaction_number']) == float(amount):
                    try:
                        db.execute(
                            "DELETE FROM account_stock WHERE stock_ticker = ? AND user_id = ?;", (stock_ticker,id))
                        db.commit()
                    except Exception as e:
                        error = f"Account_stock. Error delete the account stock: {str(e)}"
                        print(error)
                        print("AN ERROR OCCURS HERE.")
                        db.rollback()
                    try:
                        db.execute(
                            "UPDATE account SET balance = balance + ?, income += ? WHERE user_id = ?;",
                            (total_price, new_income, id)
                        )
                    except Exception as e:
                        error = f"account. Error creating account: {str(e)}"
                        db.rollback()

                else:
                    error = "Inefficient stock outstanding! Please change the selling amount."
                    flash(error)
                    return redirect(request.referrer)
            except Exception as e:
                error = f"Account_stock. Error finding the account stock: {str(e)}"
                print(error)
                print("AN ERROR OCCURS HERE.")
                db.rollback()
            # add the transaction to the transaction table
            try:
                db.execute(
                    "INSERT INTO transactions(user_id, account_id, stock_ticker, trade_type, transaction_number, buy_price,  price_per_share) VALUES (?, ?, ?, ?, ?, ?, ?);",
                    (id, account['account_id'], stock_ticker,
                     'Sell', amount, total_price, current_price)
                )
                db.commit()
            except Exception as e:
                error = f"Transactions. Error creating account: {str(e)}"
                print(error)
                db.rollback()

            try:
                account_stocks = db.execute(
                    'SELECT account_stock.user_id, account_stock.account_id, account_stock.stock_ticker, transaction_number, cost '
                    'FROM account_stock '
                    'WHERE account_stock.user_id = ?', (id,)
                ).fetchall()
            except Exception as e:
                error = f"account stocks select. Error creating account: {str(e)}"
                db.rollback()
            
            stocks = []
            total_value = 0
            balance = account['balance']
            # charting for the pie chart
            # stocks_info = {'labels':[], 'weights':[], 'values':[]}
            labels = []
            weights = []
            values = []
            for i in account_stocks:
                symbol = i['stock_ticker']
                labels.append(symbol)
                ticker = Ticker(symbol)
                current_price = ticker.price[symbol]['regularMarketPrice']
                i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
                i_dict['current_price'] = current_price
                i_dict['total_value'] = current_price * float(i['transaction_number'])
                weights.append(float(i['transaction_number']))
                values.append(current_price * float(i['transaction_number']))
                total_value += current_price * float(i['transaction_number'])
                stocks.append(i_dict)
            # print(i_dict)
        
            stocks_info = {'labels':labels, 'weights':weights, 'values': values}

            # return render_template('myacct/myportfolio.html', user=user, stocks=stocks,total_value=total_value,balance=balance, stocks_info=stocks_info)
            return redirect(url_for('stock.success', id=g.user['id']))

    return render_template('stock/sell.html', stock_ticker=stock_ticker)


@bp.route('/<int:id>/<stock_ticker>/detail')
@login_required
# may need to eliminate parameter id later for better performance
def detail(id, stock_ticker):
    db = get_db()
    stock = get_db().execute(
        'SELECT stock_ticker, company_name, company_address, company_brief, current_price, pe_ratio'
        ' FROM stock'
        ' WHERE stock_ticker = ?',
        (stock_ticker,)
    ).fetchone()

    # print(stock)

    if not stock:
        ticker = Ticker(stock_ticker)
        company_name = ticker.price[stock_ticker]['longName']
        company_brief = ticker.asset_profile[stock_ticker]['longBusinessSummary']
        current_price = ticker.price[stock_ticker]['regularMarketPrice']
        
        try:
            company_address = ticker.asset_profile[stock_ticker]['address1']
        except Exception as e:
            company_address = '-'
        try:
            pe_ratio = ticker.index_trend[stock_ticker]['peRatio']
        except Exception as e:
            pe_ratio = 0
    
    try:
        db = get_db()
        db.execute(
            "INSERT INTO stock(stock_ticker, company_name, company_address, company_brief, current_price, pe_ratio) VALUES(?, ?, ?, ?, ?, ?)",
            (stock_ticker, company_name, company_address,
             company_brief, current_price, pe_ratio)
        )
        db.commit()
    except Exception as e:
        error = f"Error creating account: {str(e)}"

    # need to refactor for better performance
    stock = get_db().execute(
        'SELECT stock_ticker, company_name, company_address, company_brief, current_price, pe_ratio'
        ' FROM stock'
        ' WHERE stock_ticker = ?',
        (stock_ticker,)
    ).fetchone()

    # ticker = Ticker(stock_ticker, asynchronous=True)
    # df = ticker.history(period='5y', interval='1d')
    # stock_html = df.to_html(index=True)

    # ticker_index = Ticker('spy', asynchronous=True)
    # df = ticker_index.history(period='5y', interval='1d')
    # index_html = df.to_html(index=True)

    end_date = datetime.now().strftime('%Y-%m-%d')
    start_date = (datetime.now() - timedelta(days=365*5)).strftime('%Y-%m-%d')
    stock_prices = yq.Ticker(stock_ticker).history(start=start_date, end=end_date, interval='1d')['adjclose']
    date = stock_prices.index.values.tolist()[1:]
    for i in range(len(date)):
        date[i] = date[i][1].strftime('%Y-%m-%d')
    prices = stock_prices.values[:].tolist()

    SPY_prices = yq.Ticker('spy').history(start=start_date, end=end_date, interval='1d')['adjclose']
    SPY = SPY_prices.values[:].tolist()

    info = {'date': date, 'prices': prices, 'SPY': SPY}

    return render_template('stock/detail.html', stock=stock, info = info)


@bp.route('/market')
@login_required
def market():
    market_info = yq.get_market_summary()
    return render_template('stock/market.html', market_info=market_info)


@bp.route('/<int:id>/alltransactions', methods=('GET', 'POST'))
@login_required
def all_transactions(id):

    if request.method == 'GET':
        db = get_db()
        try:
            transactions = db.execute('SELECT * FROM transactions WHERE user_id = ?;', (id,)).fetchall()
            db.commit()
        except Exception as e:
            error = f"transactions select. Error finding transactions: {str(e)}"
            print(error)
            db.rollback()

        return render_template('myacct/transactions.html', transactions=transactions)


    if request.method == 'POST':
        date = request.form['date']
        error = None

        if not date:
            error = 'Please pick a date.'

        if error is not None:
            flash(error)
            return redirect(request.referrer)
        else:
            db = get_db()
            try:
                transactions = db.execute(
                    'SELECT * FROM transactions WHERE user_id =? AND DATE(trade_time) = ?;', (id, date)
                ).fetchall()
                db.commit()
            except Exception as e:
                error = f"transactions select. Error finding transactions: {str(e)}"
                print(error)
                db.rollback()

    return render_template('myacct/transactions.html', transactions=transactions)


@bp.route('/<int:id>/success', methods=('GET', 'POST'))
@login_required
def success(id):
    return render_template('stock/success.html')
