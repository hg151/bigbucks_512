from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
from werkzeug.security import check_password_hash, generate_password_hash

import yahooquery as yq

from flaskr.auth import login_required
from flaskr.db import get_db

from flaskr.stock import search


bp = Blueprint('home', __name__)


@bp.route('/')
def homepage():
    return render_template('home.html')

@bp.route('/search', methods=('GET', 'POST'))
def stock():
    result = search()
    return result

@bp.route('/market')
@login_required
def market():
    market_info = yq.get_market_summary()
    return render_template('stock/market.html', market_info=market_info)

@bp.route('/transaction')
def transaction():
    return render_template('transaction/index.html')


# @bp.route('/error')
# def error():
#     return render_template('error/error.html')
