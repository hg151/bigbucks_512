from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

from yahooquery import Ticker
import yahooquery as yq

import pandas as pd
from datetime import datetime, timedelta
import json

import numpy as np
import pandas_market_calendars as mcal

bp = Blueprint('backtest',__name__)

def get_user(id):
    user = get_db().execute(
        'SELECT id, username, email, phone, usertype, userstatus'
        ' FROM user'
        ' WHERE id = ?',
        (id,)
    ).fetchone()

    if user is None:
        abort(404, f"Post id {id} doesn't exist.")

   # if check_user and post['author_id'] != g.user['id']:
    #    abort(403)

    return user

def prepare(id,date, if_current=False):
    # preparations for frontend presentation
    db = get_db()
    account = db.execute(
            'SELECT * FROM account_backtest WHERE user_id = ?', (id,)
            ).fetchone()
    stocks = db.execute(
            'SELECT * FROM stock_backtest WHERE user_id = ?', (id,)
            ).fetchall()
    transactions = db.execute(
            'SELECT * FROM transactions_backtest WHERE user_id = ?', (id,)
            ).fetchall()
    
    if if_current: 
        date_string = date
        date_format = '%Y-%m-%d'

        # Convert the date string to a datetime object
        date = datetime.strptime(date_string, date_format)

        # Add one day to the datetime object
        new_date = date + timedelta(days=1)

        # Convert the datetime object back to a string
        new_date_string = new_date.strftime(date_format)

        stocks = [dict(row) for row in stocks]

        for row in stocks:
        # Read the existing values into variables
            stock_ticker = row['stock_ticker']
            amount_hold = row['amount_hold']
            ticker = Ticker(stock_ticker)
            df = ticker.history(start=date, end=new_date_string,period='1d', interval='1d')
            price = float(df['adjclose'])
            total_price = int(amount_hold) * price
            row['current_value'] = total_price
    else:
        stocks = [dict(row) for row in stocks]
        for row in stocks:
            row['current_value'] = 0.0

    # for plot
    stock_tickers = [s['stock_ticker'] for s in stocks]
    stock_weights = {}
    print(stock_tickers)
    if len(stock_tickers) == 0:
        dates = 0
        prices = 0
    else:
        for s in stocks:
            stock_weights[s['stock_ticker']] = float(s['amount_hold'])
        end_date = datetime.now().strftime('%Y-%m-%d')
        start_date = date
        stock_prices = Ticker(stock_tickers).history(start=start_date, end=end_date)['adjclose']
        # calculate the portfolio value time series
        weighted_prices = 0
        for i in stock_tickers:
            new_prices = stock_prices[i].multiply([stock_weights[i]]*len(stock_prices[i]), axis=0)
            weighted_prices += new_prices
        dates = weighted_prices.index.values.tolist()
        for i in range(len(dates)):
            dates[i] = dates[i].strftime('%Y-%m-%d')
        prices = weighted_prices.values[:].tolist()

        dates = dates
    info = {'dates': dates, 'prices': prices}

    return account, stocks, transactions, info

@bp.route('/<int:id>/backtest', methods=('GET', 'POST'))
@login_required
def backtest(id):
    user = get_user(id)
    db = get_db()

    if request.method == 'POST':
        form_id = request.form['form_id']
        ### if the form submitted is for buy stocks
        if form_id == 'buy':
            ## Error handling
            symbol = request.form['ticker']
            amount = int(request.form['amount'])
            date = request.form['date']
            error = None

            if not symbol:
                error = 'Ticker is required.'

            if not amount:
                error = 'Amount is required.'
            
            
            # test if the ticker is valid
            ticker = Ticker(symbol)
            df = ticker.history(start='2022-01-01', end='2023-01-01')
            if df.empty:
                error = 'Invalid stock ticker.'
                flash(error)
                return redirect(request.referrer)

            # test if the market is open at that day
            # Specify the date you want to check
            date = pd.to_datetime(date)

            # Get the NYSE calendar
            nyse = mcal.get_calendar('NYSE')

            # Get the schedule for the specified date
            schedule = nyse.schedule(start_date=date, end_date=date)

            # Check if the market is open on the specified date
            if len(schedule) > 0:
                print(f"The stock market is open on {date}.")
            else:
                error = "The stock market is closed on that date."
            
            date_string = date.strftime('%Y-%m-%d')
            date_format = '%Y-%m-%d'
            date = datetime.strptime(date_string, date_format)
            current_date = datetime.now() - timedelta(days=1)
            print(date)
            if current_date <= date:
                error = "Date invalid !"
            if error is not None:
                flash(error)
                return redirect(request.referrer)
            else:
                db = get_db()
                account = db.execute(
                    'SELECT * FROM account_backtest WHERE user_id = ?', (id,)
                ).fetchone()

                date = date.strftime('%Y-%m-%d')
                date_string = date
                date_format = '%Y-%m-%d'

                print(date_string)

                # Convert the date string to a datetime object
                date = datetime.strptime(date_string, date_format)

                # Add one day to the datetime object
                new_date = date + timedelta(days=1)

                # Convert the datetime object back to a string
                new_date_string = new_date.strftime(date_format)

                df = ticker.history(start=date, end=new_date_string,period='1d', interval='1d')
                price = float(df['adjclose'])
                total_price = price * int(amount)

                if float(account['balance']) >= total_price:
                # add the stock to the portfolio
                    new_balance = float(account['balance']) - total_price
                    try:
                        db.execute(
                            "UPDATE account_backtest SET balance = ? WHERE user_id = ?;",
                        (new_balance, id)
                        )
                    except Exception as e:
                        error = f"account. Error creating account: {str(e)}"
                        db.rollback()
                    
                    # check if the stock ticker and the same date exits in the database
                    try:
                        result = db.execute(
                            "SELECT * FROM stock_backtest JOIN user ON stock_backtest.user_id = user.id WHERE stock_backtest.user_id = ? AND stock_ticker = ? AND purchased_time = ?;",
                            (id, symbol, date)
                        ).fetchone()
                        # if the record doesn't exist, then insert
                        if result == None:
                            try:
                                db.execute(
                                    "INSERT INTO stock_backtest(user_id, account_id, stock_ticker, amount_hold, cost, purchased_time) VALUES (?, ?, ?, ?, ?,?);",
                                    (id, account['account_id'],
                                    symbol, amount, float(total_price),date)
                                )
                                db.commit()
                            except Exception as e:
                                error = f"Account_stock. Can not insert: {str(e)}"
                                print(error)
                                print("AN ERROR OCCURS HERE.")
                                db.rollback()
                        # if the record exists, then update
                        else:
                            db.execute(
                                "UPDATE stock_backtest SET amount_hold = amount_hold + ?, cost = cost + ? "
                                "WHERE user_id = (SELECT id FROM user WHERE id = ?) AND stock_ticker = ? AND purchased_time = ?;",
                                (amount, float(total_price), id, symbol,date)
                            )
                            db.commit()
                    except Exception as e:
                        error = f"Account_stock. Error creating account: {str(e)}"
                        print(error)
                        print("THE PROBLEM IS HERE.")
                        db.rollback()

                    # add the transaction to transaction table
                    try:
                        db.execute(
                        "INSERT INTO transactions_backtest(user_id, account_id, stock_ticker, trade_type, transaction_number, total_price, price_per_share, trade_time) VALUES (?, ?, ?, ?, ?, ?, ?,?);",
                        (id, account['account_id'], symbol,
                         'Buy', amount, total_price, price,date)
                        )
                        db.commit()
                    except Exception as e:
                        error = f"Transactions. Error creating account: {str(e)}"
                        print(error)
                        db.rollback()
                    
                    account, stocks, transactions, info =  prepare(id,date)

                    return render_template('backtest/index.html',account=account, stocks=stocks, transactions=transactions, info=info)
                
                else:
                    error = "Insufficient account balance! Please renew your simulator."
                    flash(error)
                    return redirect(request.referrer)
        
        if form_id == "view":
            date = request.form['date']
            error = None
            date = pd.to_datetime(date)

            # Get the NYSE calendar
            nyse = mcal.get_calendar('NYSE')

            # Get the schedule for the specified date
            schedule = nyse.schedule(start_date=date, end_date=date)

            # Check if the market is open on the specified date
            if len(schedule) > 0:
                print(f"The stock market is open on {date}.")
            else:
                error = "The stock market is closed on {date}."

            date_string = date.strftime('%Y-%m-%d')
            date_format = '%Y-%m-%d'
            date = datetime.strptime(date_string, date_format)
            current_date = datetime.now() - timedelta(days=1)
            print(date)
            if current_date <= date:
                error = "Date invalid !"

            if error is not None:
                flash(error)
                return redirect(request.referrer)
            else:
                date = date.strftime('%Y-%m-%d')
                account, stocks, transactions, info = prepare(id,date,True)
                return render_template('backtest/index.html',account=account, stocks=stocks, transactions=transactions, info=info)
            
        if form_id == "new":
            try:
                db.execute("UPDATE account_backtest SET balance = 100000, income = 0 WHERE user_id = ?;", (id,))
                db.execute("DELETE FROM stock_backtest WHERE user_id = ?;",(id,))
                db.execute("DELETE FROM transactions_backtest WHERE user_id = ?;",(id,))
                db.commit()
            except Exception as e:
                    error = f"Error creating a new test case : {str(e)}"
                    print(error)
                    db.rollback()
            account, stocks, transactions, info = prepare(id,'2015-01-20')
            return render_template('backtest/index.html',account=account, stocks=stocks, transactions=transactions, info=info)
            # return redirect(url_for('backtest.success', id=g.user['id']))

    if request.method == "GET":
        account, stocks, transactions, info = prepare(id,'2015-01-20')
        return render_template('backtest/index.html',account=account, stocks=stocks, transactions=transactions, info=info)
        # return redirect(url_for('backtest.success', id=g.user['id']))
    
@bp.route('/backtest/<int:id>/<stock_ticker>/<time>/sell', methods=('GET', 'POST'))
@login_required
def sell(id, stock_ticker,time):
    user = get_user(id)
    db = get_db()

    if request.method == 'POST':
        amount = int(request.form['amount'])
        date = request.form['date']
        error = None

        if not amount:
            error = 'Amount is required.'
        
        # check if workday
        # Specify the date you want to check\
        
        date = pd.to_datetime(date)

        # Get the NYSE calendar
        nyse = mcal.get_calendar('NYSE')

        # Get the schedule for the specified date
        schedule = nyse.schedule(start_date=date, end_date=date)

        # Check if the market is open on the specified date
        if len(schedule) > 0:
            print(f"The stock market is open on {date}.")
        else:
            error = "The stock market is closed on that date."

        date_string = date.strftime('%Y-%m-%d')
        date_format = '%Y-%m-%d'
        date = datetime.strptime(date_string, date_format)
        current_date = datetime.now() - timedelta(days=1)
        print(date)
        if current_date <= date:
            error = "Date invalid !"
            
        if error is not None:
            flash(error)
            return redirect(request.referrer)
        else:
            ticker = Ticker(stock_ticker)
            # select account info from database
            account = db.execute(
                "SELECT account_id, balance FROM account_backtest JOIN user ON account_backtest.user_id = user.id WHERE user.id = ?", (
                    id,)
            ).fetchone()
            result = db.execute(
                "SELECT * FROM stock_backtest WHERE stock_ticker = ? AND user_id = ? AND purchased_time = ?;", (stock_ticker,id,time)).fetchone()
            date = date.strftime('%Y-%m-%d')
            date_string = date
            date_format = '%Y-%m-%d'
            # Convert the date string to a datetime object
            date = datetime.strptime(date_string, date_format)
            # Add one day to the datetime object
            new_date = date + timedelta(days=1)
            # Convert the datetime object back to a string
            new_date_string = new_date.strftime(date_format)

            df = ticker.history(start=date, end=new_date_string,period='1d', interval='1d')
            price = float(df['adjclose'])
            total_price = price * int(amount)
            new_cost = float(result['cost']) - float(total_price)
            new_income = total_price - float(result['cost']) / float(result['amount_hold']) * float(amount)
            if float(result['amount_hold']) > float(amount):
                try:
                    db.execute(
                        "UPDATE stock_backtest SET transaction_number = transaction_number - ?, cost = ? WHERE stock_ticker = ? AND user_id = ? AND purchased_time = ?;", (
                                amount, new_cost, stock_ticker, id, time)
                    )
                    db.commit()
                except Exception as e:
                    error = f"Stock_backtest.Error update the transaction number: {str(e)}"
                    print(error)
                    print("AN ERROR OCCURS HERE.")
                    db.rollback()
                try:
                    db.execute(
                        "UPDATE account_backtest SET balance = balance + ?, income += ? WHERE user_id = ?;",
                        (total_price, new_income, id)
                    )
                    db.commit()
                except Exception as e:
                    error = f"account_backtest. Error creating account: {str(e)}"
                    db.rollback()
            elif float(result['amount_hold']) == float(amount):
                print("YOU ARE HERE!")
                try:
                    db.execute(
                            "DELETE FROM stock_backtest WHERE stock_ticker = ? AND user_id = ? AND purchased_time = ?;", (stock_ticker,id,time))
                    db.commit()
                    print("successfully delete")
                except Exception as e:
                    error = f"Account_stock. Error delete the account stock: {str(e)}"
                    print(error)
                    print("AN ERROR OCCURS HERE.")
                    db.rollback()
                print("Before try")
                try:
                    db.execute(
                        "UPDATE account_backtest SET balance = balance + ?, income = income + ? WHERE user_id = ?;",
                        (total_price, new_income, id)
                    )
                    print("Before commit")
                    db.commit()
                    print("Successfully updated account")
                except Exception as e:
                    error = f"account_backtest. Error creating account: {str(e)}"
                    print(error)
                    db.rollback()
            else:
                error = "Insufficient stock outstanding! Please check the selling amount."
                flash(error)
                return redirect(request.referrer)
            
            try:
                db.execute(
                    "INSERT INTO transactions_backtest(user_id, account_id, stock_ticker, trade_type, transaction_number, total_price,  price_per_share, trade_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?);",
                    (id, account['account_id'], stock_ticker,
                     'Sell', amount, total_price, price, date)
                )
                db.commit()
            except Exception as e:
                error = f"Transactions. Error creating account: {str(e)}"
                print(error)
                db.rollback()
        account, stocks, transactions, info = prepare(id,'2015-01-20')
        # return render_template('backtest/index.html',account=account, stocks=stocks, transactions=transactions, info=info)
        return redirect(url_for('backtest.success', id=g.user['id']))
    return render_template('backtest/sell.html', stock_ticker=stock_ticker)
        
            
@bp.route('/<int:id>/backtest/success', methods=('GET', 'POST'))
@login_required
def success(id):
    return render_template('backtest/success.html')




            
