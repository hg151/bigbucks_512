DROP TABLE IF EXISTS user;
DROP table IF EXISTS account;
DROP table IF EXISTS stock;
DROP table IF EXISTS account_stock;
DROP table IF EXISTS transactions;
DROP table IF EXISTS stock_backtest;
DROP table IF EXISTS account_backtest;
DROP table IF EXISTS transactions_backtest;

CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL,
    email TEXT,
    phone TEXT,
    usertype TEXT NOT NULL DEFAULT 'U',
    userstatus INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE account (
    user_id INTEGER NOT NULL,
    account_id INTEGER PRIMARY KEY AUTOINCREMENT,
    balance FLOAT DEFAULT 0.0,
    open_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    income FLOAT DEFAULT 0.0,
    -- portfolio_cost FLOAT,
    -- portfolio_value FLOAT,
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE stock (
    stock_id INTEGER PRIMARY KEY AUTOINCREMENT,
    stock_ticker TEXT NOT NULL,
    company_name TEXT NOT NULL,
    company_brief TEXT,
    company_address TEXT,
    pe_ratio FLOAT,
    current_price FLOAT NOT NULL
);

CREATE TABLE account_stock (
    user_id INTEGER NOT NULL,
    account_id INTEGER NOT NULL,
    stock_ticker TEXT NOT NULL,
    transaction_number INTEGER NOT NULL,
    cost FLOAT NOT NULL,
    FOREIGN KEY (account_id) REFERENCES account(account_id),
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (stock_ticker) REFERENCES stock(stock_ticker)
);

CREATE TABLE transactions (
   user_id INTEGER NOT NULL, 
   account_id INTEGER NOT NULL,
   stock_ticker TEXT NOT NULL,
   trade_type TEXT NOT NULL,
   transaction_number INTEGER NOT NULL,
   buy_price FLOAT NOT NULL,
   price_per_share FLOAT NOT NULL,
   trade_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   FOREIGN KEY (user_id) REFERENCES user(id),
   FOREIGN KEY (account_id) REFERENCES account(account_id)
);


CREATE TABLE account_backtest (
    user_id INTEGER NOT NULL,
    account_id INTEGER PRIMARY KEY AUTOINCREMENT,
    balance FLOAT NOT NULL,
    income FLOAT,
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE stock_backtest (
    user_id INTEGER NOT NULL,
    account_id INTEGER NOT NULL,
    stock_ticker TEXT NOT NULL,
    amount_hold INTEGER NOT NULL,
    cost FLOAT NOT NULL,
    purchased_time TEXT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (account_id) REFERENCES account_backtest(account_id)
);

CREATE TABLE transactions_backtest (
    user_id INTEGER NOT NULL,
    account_id INTEGER NOT NULL,
    stock_ticker TEXT NOT NULL,
    trade_type TEXT NOT NULL,
    transaction_number INTEGER NOT NULL,
    total_price FLOAT NOT NULL,
    price_per_share FLOAT NOT NULL,
    trade_time TEXT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (account_id) REFERENCES account_backtest(account_id)
)