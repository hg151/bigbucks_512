from flask import Flask,render_template,request
from flask_mail import Mail,Message
# from flask_wtf import FlaskForm
# from wtforms.validators import DataRequired
# from wtforms import SubmitField,StringField,PasswordField
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flaskr.db import get_db
from yahooquery import Ticker


def send_mail(subject, to, body, stocks,total_value,balance):
    mail = Mail()
    message = Message(subject, recipients = [to], body = body)
    # message.body = render_template('emails/stocks.txt',stocks=stocks)
    message.html = render_template('emails/stocks.html',stocks=stocks,total_value=total_value,balance=balance)
    mail.send(message)

bp = Blueprint('email', __name__)
@bp.route('/<int:id>/email')
def send(id):
    db = get_db()
    account = db.execute(
        "SELECT account_id, email,balance FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
    account_stocks = db.execute(
        "SELECT * FROM account_stock WHERE account_id=?", (account['account_id'],)).fetchall()
    
    stocks = []
    total_value = 0
    balance = account['balance']
    for i in account_stocks:
        symbol = i['stock_ticker']
        ticker = Ticker(symbol)
        current_price = ticker.price[symbol]['regularMarketPrice']
        i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
        i_dict['current_price'] = current_price
        i_dict['total_value'] = current_price * float(i['transaction_number'])
        total_value += current_price * float(i['transaction_number'])
        stocks.append(i_dict)
        print(i_dict)
    
    send_mail("Your Account @ Bigbucks",account['email'],"Hello",stocks=stocks,total_value=total_value,balance=balance)
    return render_template("emails/sent.html")