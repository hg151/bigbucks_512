from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.auth import login_required
from flaskr.db import get_db

from yahooquery import Ticker


bp = Blueprint('myacct', __name__)

# @bp.route('/<int:id>/update', methods=('GET', 'POST'))
# @login_required
# def market():
#     market_info = yq.get_market_summary()
#     return render_template('stock/market.html', market_info=market_info)

# @bp.route('/transaction')
# def transaction():
#     return render_template('transaction/index.html')

def get_user(id):
    user = get_db().execute(
        'SELECT id, username, email, phone, usertype, userstatus'
        ' FROM user'
        ' WHERE id = ?',
        (id,)
    ).fetchone()

    if user is None:
        abort(404, f"Post id {id} doesn't exist.")

   # if check_user and post['author_id'] != g.user['id']:
    #    abort(403)

    return user


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    user = get_user(id)

    if request.method == 'GET':
        db = get_db()
        try:
            account = db.execute(
                "SELECT balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
                    id,)
            ).fetchone()
            account_stocks = db.execute(
                'SELECT account_stock.user_id, account_stock.account_id, account_stock.stock_ticker, account_stock.transaction_number, account_stock.cost '
                'FROM account_stock '
                'WHERE account_stock.user_id = ?', (id,)
            ).fetchall()
            num_stocks = len(account_stocks)
            print(
                f"{num_stocks} transactions fetched from the database.account stocks.")
            print(f"user id is {id}")
            stocks = []
            total_value = 0
            balance = account['balance']
            # charting for the pie chart
            # stocks_info = {'labels':[], 'weights':[], 'values':[]}
            labels = []
            weights = []
            values = []
            for i in account_stocks:
                symbol = i['stock_ticker']
                labels.append(symbol)
                ticker = Ticker(symbol)
                current_price = ticker.price[symbol]['regularMarketPrice']
                i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
                i_dict['current_price'] = current_price
                i_dict['total_value'] = current_price * float(i['transaction_number'])
                weights.append(float(i['transaction_number']))
                values.append(current_price * float(i['transaction_number']))
                total_value += current_price * float(i['transaction_number'])
                stocks.append(i_dict)
                print(i_dict)
        
            stocks_info = {'labels':labels, 'weights':weights, 'values': values}
            return render_template('myacct/setting.html', user=user, stocks=stocks,total_value=total_value,balance=balance, stocks_info=stocks_info)

        except Exception as e:
            error = f"account stocks select. Error creating account: {str(e)}"
            print(error)
            db.rollback()
        print("before return")
        

        return render_template('error/error.html')
    
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        phone = request.form['phone']
        error = None

        if not username:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE user SET email = ?, phone = ? WHERE username = ?;",
                (email, phone, username),
            )
            db.commit()
            return redirect(url_for('home.homepage'))

# @bp.route('/<int:id>/update', methods=('GET', 'POST'))
# @login_required
# def update(id):
    # user = get_user(id)

    # if request.method == 'GET':
    #     db = get_db()
    #     try:
    #         account = db.execute(
    #             "SELECT balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
    #                 id,)
    #         ).fetchone()
    #         account_stocks = db.execute(
    #             'SELECT account_stock.user_id, account_stock.account_id, account_stock.stock_ticker, account_stock.transaction_number, account_stock.cost '
    #             'FROM account_stock '
    #             'WHERE account_stock.user_id = ?', (id,)
    #         ).fetchall()
    #         num_stocks = len(account_stocks)
    #         print(
    #             f"{num_stocks} transactions fetched from the database.account stocks.")
    #         print(f"user id is {id}")
    #         stocks = []
    #         total_value = 0
    #         balance = account['balance']
    #         # charting for the pie chart
    #         # stocks_info = {'labels':[], 'weights':[], 'values':[]}
    #         labels = []
    #         weights = []
    #         values = []
    #         for i in account_stocks:
    #             symbol = i['stock_ticker']
    #             labels.append(symbol)
    #             ticker = Ticker(symbol)
    #             current_price = ticker.price[symbol]['regularMarketPrice']
    #             i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
    #             i_dict['current_price'] = current_price
    #             i_dict['total_value'] = current_price * float(i['transaction_number'])
    #             weights.append(float(i['transaction_number']))
    #             values.append(current_price * float(i['transaction_number']))
    #             total_value += current_price * float(i['transaction_number'])
    #             stocks.append(i_dict)
    #             print(i_dict)
        
    #         stocks_info = {'labels':labels, 'weights':weights, 'values': values}
    #         return render_template('blog/update.html', user=user, stocks=stocks,total_value=total_value,balance=balance, stocks_info=stocks_info)

    #     except Exception as e:
    #         error = f"account stocks select. Error creating account: {str(e)}"
    #         print(error)
    #         db.rollback()
    #     print("before return")
        
    #     return render_template('error/error.html')


#     if request.method == 'POST':
#         username = request.form['username']
#         password = request.form['password']
#         email = request.form['email']
#         phone = request.form['phone']
#         error = None

#         if not username:
#             error = 'Title is required.'

#         if not password:
#             error = 'Password is required.'

#         if error is not None:
#             flash(error)
#         else:
#             db = get_db()
#             db.execute(
#                 "UPDATE user SET password = ?, email = ?, phone = ? WHERE username = ?;",
#                 (generate_password_hash(
#                     password), email, phone, username),
#             )
#             db.commit()
#             return redirect(url_for('blog.index'))

@bp.route('/<int:id>/subscribe', methods=('GET', 'POST'))
@login_required
def subscribe(id):
    user = get_user(id)

    if request.method == 'POST':
        code = request.form['code']
        error = None

        if code != 'woshizhangyuge':
            error = 'Wrong subscription code.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE user SET usertype = 'P' WHERE id = ?;",
                (id,),
            )
            db.commit()
            return redirect(url_for('home.homepage'))

    return render_template('blog/subscribe.html', user=user)

 

@bp.route('/<int:id>/myportfolio', methods=('GET', 'POST'))
@login_required
def portfolio(id):
    user = get_user(id)

    if request.method == 'GET':
        db = get_db()
        try:
            account = db.execute(
                "SELECT balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
                    id,)
            ).fetchone()
            account_stocks = db.execute(
                'SELECT account_stock.user_id, account_stock.account_id, account_stock.stock_ticker, account_stock.transaction_number, account_stock.cost '
                'FROM account_stock '
                'WHERE account_stock.user_id = ?', (id,)
            ).fetchall()
            num_stocks = len(account_stocks)
            print(
                f"{num_stocks} transactions fetched from the database.account stocks.")
            print(f"user id is {id}")
            stocks = []
            total_value = 0
            balance = account['balance']
            # charting for the pie chart
            # stocks_info = {'labels':[], 'weights':[], 'values':[]}
            labels = []
            weights = []
            values = []
            for i in account_stocks:
                symbol = i['stock_ticker']
                labels.append(symbol)
                ticker = Ticker(symbol)
                current_price = ticker.price[symbol]['regularMarketPrice']
                i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
                i_dict['current_price'] = current_price
                i_dict['total_value'] = current_price * float(i['transaction_number'])
                weights.append(float(i['transaction_number']))
                values.append(current_price * float(i['transaction_number']))
                total_value += current_price * float(i['transaction_number'])
                stocks.append(i_dict)
                print(i_dict)
        
            stocks_info = {'labels':labels, 'weights':weights, 'values': values}
            return render_template('myacct/myportfolio.html', user=user, stocks=stocks,total_value=total_value,balance=balance, stocks_info=stocks_info)

        except Exception as e:
            error = f"account stocks select. Error creating account: {str(e)}"
            print(error)
            db.rollback()
        print("before return")
        

        return render_template('error/error.html')
    
    

# @bp.route('/<int:id>/update1', methods=('GET', 'POST'))
# @login_required
# def update1(id):
#     return render_template('blog/update1.html')

# @bp.route('/<int:id>/history', methods=('GET', 'POST'))
# @login_required
# def history(id):
#     return render_template('myacct/history.html')


@bp.route('/<int:id>/change_pwd', methods=('GET', 'POST'))
@login_required
def change_pwd(id):
    user = get_user(id)

    if request.method == 'GET':
        db = get_db()
        # try:
        account = db.execute(
            "SELECT balance FROM account JOIN user ON account.user_id = user.id WHERE user.id = ?", (
                id,)
        ).fetchone()
        account_stocks = db.execute(
            'SELECT account_stock.user_id, account_stock.account_id, account_stock.stock_ticker, account_stock.transaction_number, account_stock.cost '
            'FROM account_stock '
            'WHERE account_stock.user_id = ?', (id,)
        ).fetchall()
        num_stocks = len(account_stocks)
        print(
            f"{num_stocks} transactions fetched from the database.account stocks.")
        print(f"user id is {id}")
        stocks = []
        total_value = 0
        balance = account['balance']
        # charting for the pie chart
        # stocks_info = {'labels':[], 'weights':[], 'values':[]}
        labels = []
        weights = []
        values = []
        for i in account_stocks:
            symbol = i['stock_ticker']
            labels.append(symbol)
            ticker = Ticker(symbol)
            current_price = ticker.price[symbol]['regularMarketPrice']
            i_dict = dict(i)  # Convert 'sqlite3.Row' to dictionary
            i_dict['current_price'] = current_price
            i_dict['total_value'] = current_price * float(i['transaction_number'])
            weights.append(float(i['transaction_number']))
            values.append(current_price * float(i['transaction_number']))
            total_value += current_price * float(i['transaction_number'])
            stocks.append(i_dict)
            print(i_dict)
    
        stocks_info = {'labels':labels, 'weights':weights, 'values': values}
        return render_template('myacct/password.html', user=user, stocks=stocks,total_value=total_value,balance=balance, stocks_info=stocks_info)

        # except Exception as e:
        #     error = f"account stocks select. Error creating account: {str(e)}"
        #     print(error)
        #     db.rollback()
        # print("before return")
        

        return render_template('error/error.html')

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        error = None

        if not username:
            error = 'Title is required.'

        if not password:
            error = 'Password is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE user SET password = ? WHERE username = ?;",
                (generate_password_hash(
                    password), username),
            )
            db.commit()
            return redirect(url_for('home.homepage'))
        
