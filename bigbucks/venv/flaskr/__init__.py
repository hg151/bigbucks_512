import os

from flask import Flask, request
# from flask_script import Manager
# from flask_mail import Mail, Message
from flask_mail import Mail

def create_app(test_config=None):
    # create and configure the app

    app = Flask(__name__, instance_relative_config=True)
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    app.config['MAIL_SERVER'] = 'smtp.163.com'
    app.config['MAIL_PORT'] = 465
    app.config['MAIL_USE_SSL'] = True
    # app.config['MAIL_USE_TLS'] = False
    app.config['MAIL_USERNAME'] = 'bigbucks2023@163.com'
    app.config['MAIL_PASSWORD'] = 'FINMPBDYFSPPFDOE'
    app.config['MAIL_DEFAULT_SENDER'] = 'bigbucks2023@163.com'

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    # manager = Manager(app)
    mail = Mail()

    # Register the Mail object with Flask app instance
    mail.init_app(app)

    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import home
    app.register_blueprint(home.bp)
    
    from . import myacct
    app.register_blueprint(myacct.bp)

    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    from . import stock
    app.register_blueprint(stock.bp)
    app.add_url_rule('/', endpoint='index')

    from . import admin
    app.register_blueprint(admin.bp)
    app.add_url_rule('/', endpoint='index')

    from . import analysis
    app.register_blueprint(analysis.bp)
    app.add_url_rule('/', endpoint='analysis')

    from . import charge
    app.register_blueprint(charge.bp)
    app.add_url_rule('/', endpoint='index')

    from . import email
    app.register_blueprint(email.bp)
    app.add_url_rule('/', endpoint='index')

    # a simple page that says hello

    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    return app
