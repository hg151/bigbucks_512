from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.auth import login_required
from flaskr.db import get_db
import stripe

bp = Blueprint('charge', __name__)

# just for testing purposes
stripe_keys = {
    'secret_key': 'sk_test_4eC39HqLyjWDarjtT1zdp7dc',
    'publishable_key': 'pk_test_TYooMQauvdEDq54NiTphI7jx'
    }

stripe.api_key = stripe_keys['secret_key']

@bp.route('/<int:id>/pay', methods=['GET','POST'])
@login_required
def pay(id):
    # Amount in cents
    if request.method == 'POST':
        error = None
        try:
            amount = float(request.form['amount'])
        except:
            error = "Please enter a number."
        if not amount:
            error = 'Ticker is required.'

        if error is not None:
            flash(error)
        else:

            # customer = stripe.Customer.create(
            # email='customer@email.com',
            # source=request.form['stripeToken']
            # )

            # charge = stripe.Charge.create(
            #     customer=customer.id,
            #     amount=amount,
            #     currency='usd',
            #     description='Flask Charge'
            # )

            db = get_db()
            account = db.execute(
            "SELECT * FROM account JOIN user ON account.user_id = user.id WHERE user.id=?", (id,)).fetchone()
            balance = float(account['balance'])
            new_balance = balance + amount
            
            try:
                db.execute(
                    "UPDATE account SET balance = ? WHERE user_id = ?;",
                        (new_balance, id)
                    )
                print("The new balance is" + str(new_balance))
                db.commit()
            except Exception as e:
                error = f"account. Error creating account: {str(e)}"
                print(error)
                db.rollback()

            return render_template('charge/charge.html', amount=amount)
            
    return render_template('charge/index.html', key=stripe_keys['publishable_key'])
